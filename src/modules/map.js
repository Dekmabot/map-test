class Map {

    objects = Array

    isHovered = false
    isChanged = false

    hoveredKey = null
    selectedKeys = []

    init(data, logger) {
        this._data = data
        this._logger = logger
    }

    addObject(object) {
        let mapX = object.mapX
        let mapY = object.mapY
        let key = this.coordinateKey(mapX, mapY)

        this.objects[key] = object
    }

    removeObject(object) {
        delete this.objects[object.mapX][object.mapY]
    }

    hover(mapX, mapY) {
        let search = this.coordinateKey(mapX, mapY)
        this.isHovered = search in this.objects

        if (this.isHovered) {
            // Remove old
            if (this.hoveredKey !== null && this.hoveredKey !== search) {
                this.objects[this.hoveredKey].hovered = false;

                this.objects[search].hovered = true;
                this.hoveredKey = search
                this.isChanged = true
            } else if (this.hoveredKey === null) {
                this.objects[search].hovered = true;
                this.hoveredKey = search
                this.isChanged = true
            }
        } else if (this.hoveredKey !== null) {
            this.objects[this.hoveredKey].hovered = false;
            this.hoveredKey = null
            this.isChanged = true
        }

        // for (const key in this.objects) {
        //     if (key === search) {
        //         if (!this.objects[key].hovered) {
        //             this.objects[key].hovered = true
        //             this.hoveredKey = search;
        //             this.isChanged = true
        //         }
        //     } else if (this.objects[key].hovered) {
        //         this.objects[key].hovered = false
        //         this.isChanged = true
        //     }
        // }
    }

    select(mapX, mapY) {
        let search = this.coordinateKey(mapX, mapY)

        if (this.selectedKeys.includes(search)) {
            this.selectedKeys = this.selectedKeys.filter(item => item !== search)
            this.objects[search].selected = false
            this.isChanged = true

            this.log('object deselected')

            return;
        } else {
            this.log('now found: ' + search)
        }

        this.log('object selected')

        this.selectedKeys.push(search)

        this.objects[search].selected = true
        this.isChanged = true
    }

    selectRange(mapFromX, mapFromY, mapToX, mapToY) {

        for(let key of Object.keys(this.objects)){
            let object = this.objects[key]
            if(object.mapX >= mapFromX
                && object.mapX <=mapToX
                && object.mapY >=mapFromY
                && object.mapY <=mapToY){

                if(!this.selectedKeys.includes(key)){
                    this.selectedKeys.push(key)
                    this.objects[key].selected = true
                    this.isChanged = true
                }

            }else{
                this.selectedKeys = this.selectedKeys.filter(item => item !== key)
                this.objects[key].selected = false
                this.isChanged = true
            }
        }
    }

    coordinateKey(mapX, mapY) {
        return mapX + 'x' + mapY;
    }

    log(message) {
        this._logger.log(message)
    }
}

export default new Map();