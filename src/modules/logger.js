class Logger {

    constructor() {
        this._enables = true
    }

    log(message) {
        if (!this._enables) return;

        console.log(message)
    }
}

export default new Logger();