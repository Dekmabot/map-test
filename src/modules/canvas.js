class Canvas {
    layoutMap = null
    layoutObjects = null
    layoutHover = null
    layoutCounter = 0

    zoom = 0
    offsetX = 0
    offsetY = 0

    stepX = 40
    stepY = 40
    stepZoom = 0.1
    zoomMin = -9
    zoomMax = 9
    padding = 3

    colorRed = '#f00'
    colorBlack = '#000'

    _objectsCache = []

    selectorFromX = null
    selectorFromY = null
    selectorToX = null
    selectorToY = null

    constructor() {
    }

    init(container, width, height, logger) {
        this._container = container
        this._width = width
        this._height = height
        this._logger = logger

        this._container.width = this._width
        this._container.height = this._height

        this.layoutMap = this.addLayout('layout_map')
        this.layoutObjects = this.addLayout('layout_objects')
        this.layoutHover = this.addLayout('layout_hover')

        this.drawAll()
    }

    addLayout(id) {
        this.layoutCounter++
        let canvas = document.createElement('canvas');

        canvas.id = id;
        canvas.width = this._width;
        canvas.height = this._height;
        canvas.style.zIndex = this.layoutCounter.toString();
        canvas.style.position = "absolute";

        this._container.appendChild(canvas)

        let layout = canvas.getContext('2d')

        this.scaleLayout(layout, this.zoomValue())

        return layout
    }

    drawBorder() {
        this.layoutMap.strokeRect(0, 0, this._width - 1, this._height - 1);
    }

    drawNet() {
        for (let x = this.stepX; x < this._width; x += this.stepX) {
            this.layoutMap.beginPath();
            this.layoutMap.moveTo(this.scrollX(x), this.scrollY(0));
            this.layoutMap.lineTo(this.scrollX(x), this.scrollY(this._height));
            this.layoutMap.stroke();
        }
        for (let y = this.stepY; y < this._height; y += this.stepY) {
            this.layoutMap.beginPath();
            this.layoutMap.moveTo(this.scrollX(0), this.scrollY(y));
            this.layoutMap.lineTo(this.scrollX(this._width), this.scrollY(y));
            this.layoutMap.stroke();
        }
    }

    drawObjects() {
        for (let key in this._objectsCache) {
            this.drawObject(this._objectsCache[key])
        }
    }

    clear(layout) {
        layout.clearRect(0, 0, this._width, this._height);
    }

    drawObject(object) {
        let x = this.toX(object.mapX) + this.padding
        let y = this.toY(object.mapY) + this.padding
        let w = this.toWidthX(object.width) - this.padding * 2
        let h = this.toWidthY(object.height) - this.padding * 2

        if (object.hovered) {
            this.layoutObjects.fillRect(x, y, w, h);
        } else if (object.selected) {
            this.layoutObjects.fillStyle = this.colorRed
            this.layoutObjects.fillRect(x, y, w, h);
            this.layoutObjects.fillStyle = this.colorBlack
        } else {
            this.layoutObjects.strokeRect(x, y, w, h);
        }
    }

    addSelector(fromX, fromY, toX, toY) {
        this.selectorFromX = fromX
        this.selectorFromY = fromY
        this.selectorToX = toX
        this.selectorToY = toY

        this.clear(this.layoutHover)
        this.drawSelector()
    }
    removeSelector(){
        this.selectorFromX = null
        this.selectorFromY = null
        this.selectorToX = null
        this.selectorToY = null

        this.clear(this.layoutHover)
    }

    drawSelector() {
        this.log('selector '
            + this.selectorFromX + 'x'
            + this.selectorFromY + ' - '
            + this.selectorToX + 'x'
            + this.selectorToY + '')

        this.layoutHover.strokeRect(
            this.selectorFromX,
            this.selectorFromY,
            this.selectorToX,
            this.selectorToY
        );
    }

    zoomIn(delta, pointerX, pointerY) {
        let zoom = Math.min(this.zoomMax, this.zoom + 1)
        if (zoom === this.zoom) {
            return;
        }

        let zoomValue = (1 + zoom * this.stepZoom)

        this.offsetX += this.getOffsetOnZoom(this._width, this.offsetX, pointerX, zoom, zoomValue)
        this.offsetY += this.getOffsetOnZoom(this._height, this.offsetY, pointerY, zoom, zoomValue)

        this.setZoom(zoom, pointerX, pointerY)
    }

    zoomOut(delta, pointerX, pointerY) {
        let zoom = Math.max(this.zoomMin, this.zoom - 1)
        if (zoom === this.zoom) {
            return;
        }

        let zoomValue = this.zoomValue();

        this.offsetX -= this.getOffsetOnZoom(this._width, this.offsetX, pointerX, zoom, zoomValue)
        this.offsetY -= this.getOffsetOnZoom(this._height, this.offsetY, pointerY, zoom, zoomValue)

        this.setZoom(zoom, pointerX, pointerY)
    }

    scroll(deltaX, deltaY) {
        this.clearAll()

        this.offsetX -= deltaX
        this.offsetY -= deltaY

        this.drawAll()
    }

    zoomValue() {
        return 1 + this.zoom * this.stepZoom;
    }

    getOffsetOnZoom(size, currentOffset, pointerOffset, zoom, zoomValue) {
        let delta = size * this.stepZoom
        let percent = 100 / size * pointerOffset;
        let offset = delta * percent / 100

        this.log('zoom: ' + zoom
            + ', zoomValue: ' + zoomValue
            + ', size: ' + size
            + ', delta: ' + delta
            + ', percent: ' + percent
            + ', offset: ' + offset
            + ', current: ' + currentOffset
        )

        return offset
    }

    setZoom(value, pointerX, pointerY) {
        this.clearAll()

        this.zoom = value;

        this.scaleLayout(this.layoutMap, this.zoomValue(), pointerX, pointerY)
        this.scaleLayout(this.layoutObjects, this.zoomValue(), pointerX, pointerY)
        this.scaleLayout(this.layoutHover, this.zoomValue(), pointerX, pointerY)

        this.drawAll()
    }

    scaleLayout(layout, value, pointerX, pointerY) {
        layout.save()

        layout.setTransform(1, 0, 0, 1, 0, 0)
        layout.scale(value, value)

        layout.restore()
    }

    clearAll() {
        this.clear(this.layoutMap)
        this.clear(this.layoutObjects)
        this.clear(this.layoutHover)
    }

    drawAll() {
        this.drawBorder()
        this.drawNet()
        this.drawObjects();
        this.drawSelector();
    }

    refreshObjects() {
        this.clear(this.layoutObjects)
        this.drawObjects();
    }

    toX(value) {
        return this.scrollX(value * this.stepX)
    }

    toY(value) {
        return this.scrollY(value * this.stepY)
    }

    toMapX(value) {
        return Math.floor((value + this.offsetX) / this.zoomValue() / this.stepX)
    }

    toMapY(value) {
        return Math.floor((value + this.offsetY) / this.zoomValue() / this.stepY)
    }

    toWidthX(value) {
        return value * this.stepX * this.zoomValue()
    }

    toWidthY(value) {
        return value * this.stepY * this.zoomValue()
    }

    scrollX(x) {
        return x * this.zoomValue() - this.offsetX
    }

    scrollY(y) {
        return y * this.zoomValue() - this.offsetY
    }

    log(message) {
        this._logger.log(message)
    }
}

export default new Canvas();