class Controls {

    mouseX = 0
    mouseY = 0

    buttonLeft = false
    buttonMiddle = false
    buttonRight = false
    buttonAlt = false
    buttonShift = false
    buttonCtrl = false

    isDragging = false
    isPinching = false
    justDropped = false;

    startX = 0
    startY = 0

    init(container, Canvas, Map, logger) {
        this._container = container
        this._canvas = Canvas
        this._map = Map
        this._logger = logger

        this.listen()
    }

    listen() {
        this._container.addEventListener('click', (event) => this.click(event))
        this._container.addEventListener('dblclick', (event) => this.dblclick(event))
        this._container.addEventListener('mouseup', (event) => this.mouseup(event))
        this._container.addEventListener('mousedown', (event) => this.mousedown(event))
        this._container.addEventListener('mousemove', (event) => this.mousemove(event))
        this._container.addEventListener('wheel', (event) => this.wheel(event))
        this._container.addEventListener('drag', (event) => this.drag(event))
        this._container.addEventListener('dragend', (event) => this.dragend(event))
        this._container.addEventListener('dragenter', (event) => this.dragenter(event))
        this._container.addEventListener('dragleave', (event) => this.dragleave(event))
        this._container.addEventListener('dragover', (event) => this.dragover(event))
        this._container.addEventListener('dragstart', (event) => this.dragstart(event))
        this._container.addEventListener('drop', (event) => this.drop(event))

        this._container.addEventListener('contextmenu', (event) => this.contextmenu(event))
    }

    click(event) {
        // if (this.justDropped) {
        //     return false
        // }
        this.updateButtons(event)

        let mapX = this._canvas.toMapX(this.mouseX)
        let mapY = this._canvas.toMapY(this.mouseY)

        this._map.hover(mapX, mapY)
        if (this._map.isHovered) {
            this._map.select(mapX, mapY)
        }
        if (this._map.isChanged) {
            this._canvas.refreshObjects()
        }

        this.log('mouse event: click')
    }

    dblclick(event) {
        this.updateButtons(event)
        this.log('mouse event: dblclick')
    }

    mouseup(event) {
        this.updateButtons(event)

        if (event.button === 0) {
            this.buttonLeft = false
        }
        if (event.button === 1) {
            this.buttonMiddle = false
        }
        if (event.button === 2) {
            this.buttonRight = false
        }

        this.log('mouse event: mouseup at ' + this.mouseX + 'x' + this.mouseY)

        if (this.isDragging) {
            this.dragend(event)
            this.drop(event)

            this.isDragging = false
        }

        if (this.isPinching) {
            this.pinchend(event)
            this.pinched(event)

            this.isPinching = false
        }
    }

    mousedown(event) {
        this.updateButtons(event)

        if (event.button === 0) {
            this.buttonLeft = true
        }
        if (event.button === 1) {
            this.buttonMiddle = true
        }
        if (event.button === 2) {
            this.buttonRight = true
        }

        this.justDropped = false;

        this.log('mouse event: mousedown at ' + this.mouseX + 'x' + this.mouseY)
    }

    mousemove(event) {
        this.updateCoords(event)

        if (!this.isDragging && (this.buttonLeft || this.buttonRight)) {
            this.isDragging = true

            this.dragstart(event)
        }
        if (!this.isPinching && this.buttonMiddle) {
            this.isPinching = true

            this.pinchstart(event)
        }

        if (this.isDragging) {
            this.drag(event)
        } else if (this.isPinching) {
            this.pinching(event)
        } else {
            let mapX = this._canvas.toMapX(this.mouseX)
            let mapY = this._canvas.toMapY(this.mouseY)

            this._map.hover(mapX, mapY)
            if (this._map.isChanged) {
                this._canvas.refreshObjects()
            }
        }
    }

    wheel(event) {
        this.updateButtons(event)

        this.log('mouse event: wheel')

        // Возвращает число  горизонтальной прокрутки колеса мыши (mouse wheel) (Ось X)
        let deltaX = event.deltaX

        // Возвращает число  вертикальной прокрутки колеса мыши (mouse wheel) (Ось Y)
        let deltaY = event.deltaY

        // озвращает количество прокрутки колеса мыши по оси Z
        let deltaZ = event.deltaZ

        // Возвращает чило, представляющее единицу измерения для значений delta (DOM_DELTA_PIXEL, DOM_DELTA_PIXEL, DOM_DELTA_PAGE)
        let deltaMode = event.deltaMode

        if (this.buttonShift) {
            if (deltaY > 0) {
                return this.zoomOut(event, Math.abs(deltaY))
            } else if (deltaY < 0) {
                return this.zoomIn(event, Math.abs(deltaY))
            }
        }
    }

    drag(event) {
        let deltaX = Math.abs(this.mouseX - this.startX)
        let deltaY = Math.abs(this.mouseY - this.startY)

        let fromX = Math.min(this.startX, this.mouseX)
        let fromY = Math.min(this.startY, this.mouseY)

        let fromMapX = this._canvas.toMapX(this.startX)
        let fromMapY = this._canvas.toMapY(this.startY)
        let toMapX = this._canvas.toMapX(this.mouseX)
        let toMapY = this._canvas.toMapY(this.mouseY)

        let fMapX = Math.min(fromMapX, toMapX)
        let fMapY = Math.min(fromMapY, toMapY)
        let tMapX = Math.max(fromMapX, toMapX)
        let tMapY = Math.max(fromMapY, toMapY)

        this.log('selectRange '
            + fromMapX + 'x'
            + fromMapY + ' - '
            + toMapX + 'x'
            + toMapY + '')

        this._map.selectRange(fMapX, fMapY, tMapX, tMapY)
        this._canvas.addSelector(fromX, fromY, deltaX, deltaY)

        if (this._map.isChanged) {
            this._canvas.refreshObjects()
        }

        this.log('mouse event: drag')
    }

    dragend(event) {
        this.log('mouse event: dragend')
    }

    dragenter(event) {
        this.log('mouse event: dragenter')
    }

    dragleave(event) {
        this.log('mouse event: dragleave')
    }

    dragover(event) {
        this.log('mouse event: dragover')
    }

    dragstart(event) {
        this.startX = this.mouseX
        this.startY = this.mouseY
        this.log('mouse event: dragstart')
    }

    drop(event) {
        this.justDropped = true;
        this._canvas.removeSelector()
        this.log('mouse event: drop')
    }

    pinchstart(event) {
        this.startX = this.mouseX
        this.startY = this.mouseY
        this.log('mouse event: pinchstart at ' + this.startX + 'x' + this.startY)
    }

    pinching(event) {
        let deltaX = this.mouseX - this.startX
        let deltaY = this.mouseY - this.startY

        this._canvas.scroll(deltaX, deltaY)

        this.startX = this.mouseX
        this.startY = this.mouseY

        this.log('mouse event: pinching (+' + deltaX + ', +' + deltaY + ')')
    }

    pinchend(event) {
        this.log('mouse event: pinchend')
    }

    pinched(event) {
        let deltaX = this.mouseX - this.startX
        let deltaY = this.mouseY - this.startY

        this._canvas.scroll(deltaX, deltaY)

        this.log('mouse event: pinched (+' + deltaX + ', +' + deltaY + ')')
    }


    zoomIn(event, delta) {
        this._canvas.zoomIn(delta, this.mouseX, this.mouseY)
        this.log('mouse event: zoomIn, delta: ' + delta)
    }

    zoomOut(event, delta) {
        this._canvas.zoomOut(delta, this.mouseX, this.mouseY)
        this.log('mouse event: zoomOut, delta: ' + delta)
    }

    contextmenu(event) {
        event.preventDefault()
        return false
    }

    updateCoords(event) {
        this.mouseX = event.clientX
        this.mouseY = event.clientY
    }

    updateButtons(event) {
        this.buttonAlt = event.altKey;
        this.buttonCtrl = event.ctrlKey;
        this.buttonShift = event.shiftKey;
    }

    log(message) {
        this._logger.log(message)
    }
}

export default new Controls();