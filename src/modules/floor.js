
import * as Seat from './../objects/seat.js'

class Floor {

    hoveredObjects = []
    selectedObjects = []

    init(canvas, map, controls, logger) {
        this._canvas = canvas
        this._map = map
        this._controls = controls
        this._logger = logger

        this.log('floor initiated')
    }

    addSeat(mapX, mapY) {
        let seat = Seat.create(mapX, mapY, this._logger)

        this._map.addObject(seat)
        this._canvas.drawObject(seat)
        this._canvas._objectsCache = this._map.objects /* TODO: плохо */

        //this.objects.push(seat) /* TODO: зачем */
    }

    log(message) {
        this._logger.log(message)
    }
}

export default new Floor();