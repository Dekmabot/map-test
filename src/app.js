import Canvas from './modules/canvas.js'
import Controls from './modules/controls.js'
import Floor from './modules/floor.js'
import Map from './modules/map.js'
import Logger from './modules/logger.js'

let container = document.getElementById('app');
let data = {
    name: 'Name'
}

Canvas.init(container, 1000, 500, Logger)
Map.init(data, Logger)
Controls.init(container, Canvas, Map, Logger)

Floor.init(Canvas, Map, Controls, Logger)
for (let j = 1; j < 13; j++) {
    for (let i = 1; i < 26; i += j) {
        Floor.addSeat(i - 1, j - 1)
    }
}
