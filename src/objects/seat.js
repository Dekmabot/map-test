class Seat {

    width = 1
    height = 1
    hovered = false
    selected = false

    constructor(mapX, mapY, logger) {
        this.mapX = mapX
        this.mapY = mapY
        this._logger = logger
    }

    log(message) {
        this._logger.log(message)
    }
}

export function create(mapX, mapY, logger) {
    return new Seat(mapX, mapY, logger)
}